package com.example.celsuiutofahrenheit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Converter extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String line = req.getParameter("temp");
        if (line != null) {
            float temp = Float.valueOf(req.getParameter("temp"));

            if (req.getParameter("direction").equals("1")) {
                resp.getWriter().println("<p>" + (temp * 1.8 + 30) + "</p>");
            } else {
                resp.getWriter().println("<p>" + (temp-30)/1.8 + "</p>");
            }
        }

    }
}
